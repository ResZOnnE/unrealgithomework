// Copyright Epic Games, Inc. All Rights Reserved.

#include "Homework2GameMode.h"
#include "Homework2HUD.h"
#include "Homework2Character.h"
#include "UObject/ConstructorHelpers.h"

AHomework2GameMode::AHomework2GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AHomework2HUD::StaticClass();
}
